﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] animalPrefabs;
    private float spawnRangeX = 20;
    private float spawnPosZ = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //posicion aleatoria en x del animal que vamos a spawnear
            float xPos = Random.Range(-spawnRangeX, spawnRangeX);
            Vector3 spawnPos = new Vector3(xPos, 0, spawnPosZ);

            //elejimos un indice aleatorio para el array
            int animalIndex = Random.Range(0,animalPrefabs.Length);
            //elejimos un animal aleatorio de la lista
            GameObject randomAnimal = animalPrefabs[animalIndex];
            //instanciamos el animal
            Instantiate(randomAnimal, spawnPos, randomAnimal.transform.rotation);
        }
    }
}
