﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    public float speed = 10f;
    private float horizontalInput;
    public float xRange = 15;
    private float verticalInput;
    public float zRange = 0.5f;
    Vector3 currentOffset;
    public GameObject projectilePrefabs;
    public GameObject projectilePrefabs2;
    public float number = 0;
    // Start is called before the first frame update
    void Start()
    {
        currentOffset = transform.position;

       // offset.x+xRange
       //offset.y+xRange

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)&&number %2!=0)
        {
            Instantiate(projectilePrefabs, transform.position, projectilePrefabs.transform.rotation);
            number++;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && number % 2 == 0)
        {
            Instantiate(projectilePrefabs2, transform.position, projectilePrefabs2.transform.rotation);
            number++;
        }



            //movimiento horizontal
            horizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * speed * Time.deltaTime * horizontalInput);

        //Entra en el if cuando me salgo por la izquierda con posicion absoluta
      //  if(transform.position.x < -xRange)
      //  {
      //      transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
     //   }
        //Entra en el if cuando me salgo por la izquierda con posicion relativa
        if (transform.position.x < currentOffset.x -xRange)
        {
            transform.position = new Vector3(currentOffset.x-xRange, transform.position.y, transform.position.z);
        }

        //Entra en el if cuando me salgo por la derecha con posicion absoluta
    //    if (transform.position.x > +xRange)
    //    {
    //       transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
    //    }

        //Entra en el if cuando me salgo por la derecha con posicion relativa
        if (transform.position.x > currentOffset.x + xRange)
        {
            transform.position = new Vector3( currentOffset.x+xRange, transform.position.y, transform.position.z);

        }
        //movimiento vertical
        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.forward * speed * Time.deltaTime * verticalInput);
        //Entra en el if cuando me salgo por atras con posicion absoluta
    //    if ( transform.position.z < -zRange)
    //    {
     //       transform.position = new Vector3(transform.position.x, transform.position.y, -zRange);
   //     }
        //Entra en el if cuando me salgo por atras con posicion relativa
        if (transform.position.z <currentOffset.z -zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,currentOffset.z -zRange);
        }
        //  Entra en el if cuando me salgo por delante con posicion absoluta
     //   if (transform.position.z > zRange)
     //   {
     //       transform.position = new Vector3(transform.position.x, transform.position.y, zRange);
     //  }
        //  Entra en el if cuando me salgo por delante con posicion relativa
        if (transform.position.z >currentOffset.z +zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y,currentOffset.z +zRange);

        }
    }
}
